# Site-Twice


![GitHub repo size](https://shields.io/github/repo-size/Nicholas-Goes/ProjetoSite-Twice?style=for-the-badge)
![GitHub language count](https://img.shields.io/github/languages/count/Nicholas-Goes/ProjetoSite-Twice?style=for-the-badge)

<img src="https://i.pinimg.com/originals/9a/84/04/9a8404370d4dcb35d90ec88a5bcd060e.jpg" alt="exemplo imagem">

> Projeto Utilizado Para Estudo de Layout usando como Tema o Grupo Sul-Coreano TWICE.

### Ajustes e melhorias

O projeto ainda está em desenvolvimento e as próximas atualizações serão voltadas nas seguintes tarefas:

- [x] Criação do Html & Css Base
- [x] Adição de Textos
- [x] Adição de Imagens
- [x] Responsividade & Revisão dos Textos


[⬆ Voltar ao topo](#Site-Twice)<br>
